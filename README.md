# Monitor

Ansible Role to deploy Monitor stack using Docker Compose:

- nginx
- Grafana + prometheus
- Graylog 